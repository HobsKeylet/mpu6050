#! /usr/bin/env python3
# -*- coding:utf-8 -*-

import smbus
import math
import time
import rospy, tf
import tf2_ros
from sensor_msgs.msg import *
from geometry_msgs.msg import *

#アドレスの設定
DEV_ADDR = 0x68
ACCEL_XOUT = 0x3b
ACCEL_YOUT = 0x3d
ACCEL_ZOUT = 0x3f
GYRO_XOUT = 0x43
PWR_MGMT_1 = 0x6b
PWR_MGMT_2 = 0x6c

bus = smbus.SMBus(1)
bus.write_byte_data(DEV_ADDR, PWR_MGMT_1, 0)

def read_word(adr):
	high = bus.read_byte_data(DEV_ADDR, adr)
	low = bus.read_byte_data(DEV_ADDR, adr+1)
	val = (high << 8) + low
	return val

def read_word_sensor(adr):
	val = read_word(adr)
	if (val >= 0x8000):#minus
		return -((65535 - val) + 1)
	else:#plus
		return val

def getGyro():
	x = read_word_sensor(GYRO_XOUT)/131.0
	y = read_word_sensor(GYRO_YOUT)/131.0
	z = read_word_sensor(GYRO_ZOUT)/131.0

	return [x, y, z]

def getAccel():
	x =read_word_sensor(ACCEL_XOUT)/16384.0
	y =read_word_sensor(ACCEL_YOUT)/16384.0
	z =read_word_sensor(ACCEL_ZOUT)/16384.0
  	
	return [x, y, z]

def calcEuler(x,y,z):
	theta = math.atan( x / math.sqrt(y*y + z*z) )
	psi = math.atan( y / math.sqrt(x*x + z*z) )
	phi = math.atan( math.sqrt( x*x + y*y ) / z)
	return [theta, psi, phi]

def euler2quaternion(x,y,z):
	q = tf.transformations.quaternion_from_euler(x,y,z)
	return Quaternion(x=q[0], y=q[1], z=q[2], w=q[3])

if __name__ == "__main__":
	rospy.init_node("mpu6050")
	br = tf2_ros.TransformBroadcaster()

	r = rospy.Rate(10)

	try:
		while not rospy.is_shutdown():
			ax, ay, az = getAccel()

			roll, pitch, yaw = calcEuler(ax,ay,az)

			q = euler2quaternion(roll,pitch,yaw)	#オイラー角をクォータ二オンに変換

			br.sendTransform((0,0,2.0), tf.transformations.quaternion_from_euler(roll,pitch,yaw), rospy.Time.now(), "/pod1/imu", "map")

			print("Roll:[%s]"%str(roll))
			print("Pitch:[%s]"%str(pitch))
			print("Yaw:[%s]"%str(yaw))

			r.sleep()

	except KeyboardInterrupt:
		print("Ctrl + c")
