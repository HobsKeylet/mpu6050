#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
from std_msgs.msg import String

import time

from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3
import tf
import math

#--- 角度のオーバー時の調整
def angle_fix(e):
    ang = e
    while ang>math.pi:
        ang-=math.pi*2
    while ang<-math.pi:
        ang+=math.pi*2
    return ang

#--- Quaternionからオイラー角に変更
# quaternion : Quaternion
# return : [x, y, z]
def quaternion_to_euler(quaternion):
    e = tf.transformations.euler_from_quaternion((quaternion.x, quaternion.y, quaternion.z, quaternion.w))
    fix = [e[0], e[1], e[2]]
    for i in range(3):
        fix[i] = angle_fix(fix[i])

    return fix

def calcEuler(x,y,z):
        theta = math.atan2(x, math.sqrt(y*y + z*z))
        psi = math.atan2(y, math.sqrt(x*x + z*z))
        phi = math.atan2(math.sqrt( x*x + y*y ), z)

        #deg_theta = math.degrees( theta )
        #deg_psi = math.degrees( psi )
        #deg_phi = math.degrees( phi )

        #return [math.degrees( theta ), math.degrees( psi ), math.degrees( phi>
        return [theta, psi, phi]


class testNode():
    def __init__(self):
        # Subscriberの作成
        self.sub1 = rospy.Subscriber("/pod1/imu/data_raw", Imu, self.callback1)
        self.sub2 = rospy.Subscriber("/pod1/imu/data", Imu, self.callback2)
        # Publisherの作成
        self.pub1 = rospy.Publisher('/pod1/imu/data_raw/rpy', Vector3, queue_size=1)
        self.pub2 = rospy.Publisher('/pod1/imu/data/rpy', Vector3, queue_size=1)

    def callback1(self, msg):
        data = Vector3()
        rpy = quaternion_to_euler(msg.orientation)
        #
	data.x = rpy[0]*180/math.pi
        data.y = rpy[1]*180/math.pi
        data.z = rpy[2]*180/math.pi
	
	print(data)
        self.pub1.publish(data)

    def callback2(self, msg):
        data = Vector3()
        rpy = quaternion_to_euler(msg.orientation)
        data.x = rpy[0]*180/math.pi
        data.y = rpy[1]*180/math.pi
        data.z = rpy[2]*180/math.pi

	print(data)
        self.pub2.publish(data)
        
if __name__ == '__main__':
    rospy.init_node('test_node')

    time.sleep(1.0)
    node = testNode()

    while not rospy.is_shutdown():
        rospy.sleep(0.1)
